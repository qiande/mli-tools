# mli-tools
 
在线openapi转换工具，主要将OpenAPI 3.0 转换成OpenAPI 2.0

全局安装：
npm install -g api-spec-converter

 **启动：** 
前端:
yarn install
yarn start
or 
npm istall 
npm run dev 
npm run build:prod


 **部署：** 
Maven打包：mvn clean compile package
后台：deploy/start.sh

import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'umi'
import { Button, Row, Input, Form, Radio, Col } from 'antd'
import { Trans } from '@lingui/macro'
import { request } from 'utils'

import styles from './index.less'

const FormItem = Form.Item
// const baseUrl = 'http://suzmlicms01.exigengroup.com:8801'
const baseUrl = 'http://localhost:8801'

@connect(({ loading, dispatch, dxp2 }) => ({ loading, dispatch, dxp2 }))
class ToDxp extends PureComponent {
  state = {
    routes: '',
    dxpJson: '',
    functions:''
  }
  formRef = React.createRef()
  render() {
    const { loading, convertType, url, routes, dxpJson,functions } = this.props

    const handleOk = (values) => {
      const fields = this.formRef.current.getFieldsValue()
      this.setState({ routes: '', dxpJson: '',functions:'' })
      console.log('process.env', process.env)
      request({
        url: `${baseUrl}/api/users/toDxp`,
        method: 'post',
        data: {
          type: fields.convertType, //'training',
          url: fields.url, // 'http://deqian-i7:9081/v3/api-docs',
        },
      }).then((res) => {
        this.setState({ routes: res.routes, dxpJson: res.dxpJson ,functions:res.functions})
      })
    }

    //
    const options = [
      { value: 'training', label: 'training' },
      { value: 'hrm', label: 'hrm' },
      { value: 'channel', label: 'channel' },
      { value: 'commission', label: 'commission' },
      { value: 'serviceQuality', label: 'service_quality' },
      { value: 'billing', label: 'billing' },
    ]
    return (
      <Fragment>
        <div className={styles.form}>
          <Form
            ref={this.formRef}
            initialValues={{ convertType, url, routes, dxpJson ,functions}}
          >
            <FormItem
              name="url"
              rules={[{ required: true }]}
              label="BE API"
              hasFeedback
            >
              <Input placeholder="https://cms-training-mli-dev01.dev.aws06.mlic.cloud/v3/api-docs" />
            </FormItem>
            <FormItem
              name="convertType"
              rules={[{ required: true }]}
              label="Module"
              hasFeedback
            >
              <Radio.Group>
                {options.map((item) => {
                  return <Radio value={item.value}>{item.label}</Radio>
                })}
              </Radio.Group>
            </FormItem>
          </Form>
          <Row>
            <Button
              type="primary"
              size="large"
              style={{ width: 100 }}
              loading={loading.effects.login}
              onClick={handleOk}
            >
              <Trans>Converter</Trans>
            </Button>
          </Row>
          <Row>
            <Col span={24}>
              <div class="ant-col ant-form-item-label">
                <label
                  for="url"
                  class="ant-form-item-required"
                  title="BE API(copy to mli.cms.xxx.module.routes)"
                >
                  BE API(copy to mli.cms.xxx.module.routes)
                </label>
              </div>
              <Input.TextArea
                readOnly
                name="routes"
                value={this.state.routes}
                style={{ height: 120 }}
              />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <div class="ant-col ant-form-item-label">
                <label
                  for="url"
                  class="ant-form-item-required"
                  title="DXP API(copy to mli.persona.xxx.json)"
                >
                  DXP API(copy to mli.persona.xxx.json)
                </label>
              </div>
              <Input.TextArea
                readOnly
                value={this.state.dxpJson}
                style={{ height: 400 }}
              />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <div class="ant-col ant-form-item-label">
                <label
                  for="function"
                  class="ant-form-item-required"
                  title="function"
                >
                  function
                </label>
              </div>
              <Input.TextArea
                readOnly
                value={this.state.functions}
                style={{ height: 400 }}
              />
            </Col>
          </Row>
        </div>
        <div className={styles.footer}></div>
      </Fragment>
    )
  }
}

ToDxp.propTypes = {
  form: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default ToDxp

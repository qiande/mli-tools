/* Copyright © 2022 EIS Group and/or one of its affiliates. All rights reserved. Unpublished work under U.S. copyright laws.
CONFIDENTIAL AND TRADE SECRET INFORMATION. No portion of this work may be copied, distributed, modified, or incorporated into any other media without EIS Group prior written consent.*/
package com.mli.tools.system.domain.vo;

import lombok.Data;

/**
 * TODO description
 *
 * @author dqian
 * @since
 */
@Data
public class DxpResultDto {
    private String routes;
    private String dxpJson;
    private String functions;
}
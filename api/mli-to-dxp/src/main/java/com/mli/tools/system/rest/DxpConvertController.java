
package com.mli.tools.system.rest;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.mli.tools.config.FileProperties;
import com.mli.tools.config.annotation.AnonymousAccess;
import com.mli.tools.system.domain.vo.ConvertToDxpVo;
import com.mli.tools.system.domain.vo.DxpResultDto;
import com.mli.tools.utils.ShellUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Qian Duo Duo
 * @date 2018-11-23
 */
@Api(tags = "DXP转换工具")
@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class DxpConvertController {
    public static final String CHARSET_NAME = "UTF-8";
    ;
    /**
     * 文件配置
     */
    private final FileProperties properties;

    @ApiOperation("查询")
    @GetMapping
    @AnonymousAccess
    public ResponseEntity<Object> query() {

        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @AnonymousAccess
    @ApiOperation("转换到DXP")
    @PostMapping(value = "/toDxp")
    public ResponseEntity<Object> toDxp(@RequestBody ConvertToDxpVo toDxpVo) throws Exception {
//        List<String> unusedList = new ArrayList<>();
//        if (CollectionUtils.isEmpty(unusedList)) {
//            try {
//                Resource templateResource = new org.springframework.core.io.ClassPathResource("unusedlist.txt");
//                IoUtil.readLines(templateResource.getInputStream(), CharsetUtil.charset(CHARSET_NAME), unusedList);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmssS");
        String fileName = "swagger-" + format.format(date) + ".json";

        File saveFile = new File(properties.getPath().getPath());
        String filePath = saveFile.getPath() + File.separator + fileName;
        String cmd = "api-spec-converter --from=openapi_3 --to=swagger_2 --syntax=json --order=openapi "
                + toDxpVo.getUrl() + " > " + filePath;
        // Windows || linux
        String result = ShellUtils.execCmdOrder(cmd, saveFile);

        File executeFile = new File(filePath);
        Object putRet = null;
        if (executeFile.exists()) {
            putRet = JSON.parseObject(ShellUtils.readFile(executeFile), Feature.OrderedField);
        } else {
            putRet = JSON.parseObject(result, Feature.OrderedField);
        }
        DxpResultDto resultDto = new DxpResultDto();

        System.out.println("convert start:" + toDxpVo.getUrl());
        if (putRet != null) {
            JSONObject jsonObject = (JSONObject) putRet;
            jsonObject.put("x-dxp-spec", JSON.parseObject("{\"version\":1.0,\"imports\":{\"census.class\":{\"schema\":\"mli.integration." + toDxpVo.getType() + ".json\",\"path-filter\":\"exclude-all\"}}}"));
            JSONObject paths = (JSONObject) (jsonObject).get("paths");
            List<String> removeList = new ArrayList<>();
            List<String> routeList = new ArrayList<>();
            StringBuilder sbFunctions = new StringBuilder();
            paths.keySet().forEach(key -> {
                //移除相关接口
                if (key.contains("/batch/batchCreate")
                        || key.contains("/batch/batchDelete")
                        || key.contains("/dump")
                        || key.contains("/metadata")
                        || key.contains("/loadLatest")) {
                    removeList.add(key);
                    return;
                }

//                //通过unusedlist.txt 移除未使用的接口
//                Optional<String> optional = unusedList.stream().filter(unused -> unused.contains(key)
//                        && !unused.contains("/batch/")
//                        && !unused.contains("/dxp/")).findAny();
//                if (optional.isPresent()) {
//                    removeList.add(key);
//                    return;
//                }
                JSONObject requestJson = (JSONObject) paths.get(key);
                requestJson.keySet().forEach(item -> {
                    try {
                        JSONObject data = (JSONObject) requestJson.get(item);
                        Object summary = data.get("summary");
                        Object operationId = data.get("operationId");

                        JSONArray jsonArray = (JSONArray) data.get("tags");
                        if (jsonArray.size() > 0) {
                            String tag = jsonArray.get(0).toString();
                            //去掉带有非法字符的方法 如：-
                            if (tag.indexOf("-") > 0) {
                                removeList.add(key);
                                return;
                            }
                            String viewName = tag.toLowerCase();
                            if (!routeList.contains(viewName)) {
                                routeList.add(viewName);
                            }
                            //增加API summary
                            String newRoute = viewName.replace("entity", "");
                            String newUrl = String.format("/%s/%s", toDxpVo.getType(), toDxpVo.getType().toLowerCase() + newRoute);
                            if (summary != null) {
                                sbFunctions.append("\"").append(summary.toString()).append(",").append(newUrl).append(key).append("\",").append("\r\n");
                            } else {
                                sbFunctions.append("\"").append(operationId.toString()).append(",").append(newUrl).append(key).append("\",").append("\r\n");
                            }

                            String newTags = "/" + toDxpVo.getType() + "/" + tag;
                            data.put("tags", JSON.parseArray("[\"" + newTags + "\"]"));
                        }
                    } catch (Exception e) {
                        System.out.println("Exception:" + e);
                    }
                });
            });
            System.out.println("remove url QTY:" + removeList.size());
            removeList.forEach(item -> {
                paths.remove(item);
            });
            resultDto.setDxpJson(JSON.toJSONString(jsonObject));
            if (!routeList.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("->        /                                           mli.cms." + toDxpVo.getType() + ".Routes");
                sb.append("\r\n");
                for (String route : routeList) {
                    String newRoute = route.replace("entity", "");
                    sb.append(String.format("-> /%s/%s/            mli.mlicms%s.%s.generated.Routes"
                            , toDxpVo.getType()
                            , toDxpVo.getType().toLowerCase() + newRoute
                            , toDxpVo.getType().toLowerCase()
                            , toDxpVo.getType().toLowerCase() + route));
                    sb.append("\r\n");
                }
                resultDto.setRoutes(sb.toString());
            }
            resultDto.setFunctions(sbFunctions.toString());
        }
        System.out.println("dxp success:" + toDxpVo.getUrl());
        if (executeFile.exists()) {
            FileUtil.del(executeFile);
        }
        return new ResponseEntity<>(resultDto, HttpStatus.OK);
    }

}

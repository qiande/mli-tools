
package com.mli.tools.system.domain.vo;

import com.mli.tools.system.enums.ConvertTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 修改密码的 Vo 类
 *
 * @author Qian Duo Duo
 * @date 2019年7月11日13:59:49
 */
@Data
public class ConvertToDxpVo {

    @NotEmpty(message="url Cannot Be Empty")
    private String url;

    @NotEmpty(message="type Cannot Be Empty")
    private String type;
}

package com.mli.tools.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ：zhengxu
 * @date ：Created in 2022/08/30
 * @since：SP
 */
@Getter
@AllArgsConstructor
public enum ConvertTypeEnum {

    training("training", "/training/"),

    hrm("hrm", "/hrm/"),
    channel("channel", "/channel/"),
    commission("commission", "/commission/"),

    service_quality("service_quality", "/servicequality/");

    private final String code;
    private final String description;

    public static ConvertTypeEnum find(Integer code) {
        for (ConvertTypeEnum value : ConvertTypeEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }
}

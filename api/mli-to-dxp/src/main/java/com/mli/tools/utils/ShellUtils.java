/* Copyright © 2022 EIS Group and/or one of its affiliates. All rights reserved. Unpublished work under U.S. copyright laws.
CONFIDENTIAL AND TRADE SECRET INFORMATION. No portion of this work may be copied, distributed, modified, or incorporated into any other media without EIS Group prior written consent.*/
package com.mli.tools.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * TODO description
 *
 * @author dqian
 * @since
 */
public class ShellUtils {

    public static String execCmdOrder(String cmdCommand, File saveFile) {

        if (StringUtils.isEmpty(cmdCommand)) {
            throw new RuntimeException("需要执行的命令不可以为空");
        }
        System.out.println("exce command:" + cmdCommand);
        /*if (***其他判断条件***){
            throw new RuntimeException("不满足判断条件的原因");
        }*/
        //组装cmd 命令
        String cmd = cmdCommand;
        BufferedReader br = null;
        Process process = null;
        //执行命令结果
        StringBuilder result = null;
        try {
            //执行cmd命令
            String[] cmdarray;
            if (isLinuxOS()) {
                cmdarray = new String[]{"/bin/bash", "-c", cmd};
            } else {
                cmdarray = new String[]{"cmd", "/c", cmd};
            }

            process = Runtime.getRuntime().exec(cmdarray, null, saveFile);
            //获取cmd命令的输出结果
            br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            result = new StringBuilder();
            String tmp;
            //组装命令执行结束后返回值
            while ((tmp = br.readLine()) != null) {
                result.append(tmp).append("\n");
            }
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (process != null) {
                process.destroy();
            }
            if (result.length() > 200) {
                System.out.println("shell result：" + result.substring(0, 200));
            } else {
                System.out.println("shell result：" + result);
            }

        }
        return result.toString();
    }


    public static String execCmdOrder2(String cmdCommand) {

        if (StringUtils.isEmpty(cmdCommand)) {
            throw new RuntimeException("需要执行的命令不可以为空");
        }

        /*if (***其他判断条件***){
            throw new RuntimeException("不满足判断条件的原因");
        }*/

        //组装cmd 命令
        String cmd = cmdCommand;
        BufferedReader br = null;
        Process process = null;
        //执行命令结果
        StringBuilder result = null;
        try {
            //执行cmd命令
            process = Runtime.getRuntime().exec(cmd);
            //获取cmd命令的输出结果
            br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            result = new StringBuilder();
            String tmp;
            //组装命令执行结束后返回值
            while ((tmp = br.readLine()) != null) {
                result.append(tmp).append("\n");
            }
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (process != null) {
                process.destroy();
            }
            System.out.println("执行命令结束以后控制台返回结果为 ：" + result);
        }
        return result.toString();
    }


    /**
     * 将文件中的sql语句以；为单位读取到列表中
     *
     * @param sqlFile /
     * @return /
     * @throws Exception e
     */
    public static String readFile(File sqlFile) throws Exception {

        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(sqlFile), StandardCharsets.UTF_8))) {
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                sb.append(tmp);
            }
        }

        return sb.toString();
    }


    public static Boolean isLinuxOS() {
        String os = System.getProperty("os.name");
        //Windows操作系统
        if (os != null && os.toLowerCase().startsWith("windows")) {
            System.out.println(String.format("当前系统版本是:%s", os));

        } else if (os != null && os.toLowerCase().startsWith("linux")) {//Linux操作系统
            System.out.println(String.format("当前系统版本是:%s", os));
            return true;
        } else { //其它操作系统
            System.out.println(String.format("当前系统版本是:%s", os));
        }
        return false;
    }
}
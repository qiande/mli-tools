
package com.mli.tools.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Qian Duo Duo
 * @description
 * @date 2021-11-22
 **/
@Data
//@Component
public class IpchapterProperties {

    public static Boolean ipLocal;

//    @Value("${ip.local-parsing}")
    public void setIpLocal(Boolean ipLocal) {
        IpchapterProperties.ipLocal = ipLocal;
    }
}
